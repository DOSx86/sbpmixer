# SBPmixer

Sound Blaster Pro Mixer Chip Interface command line utility to view and change mixer settings on compatible cards. Also includes, a program loadable/embeddable driver to control the mixer chip.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SBPMIXER.LSM

<table>
<tr><td>title</td><td>SBPmixer</td></tr>
<tr><td>version</td><td>2.0</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-05-30</td></tr>
<tr><td>description</td><td>Mixer for Sound Blaster Pro compatible cards</td></tr>
<tr><td>summary</td><td>Sound Blaster Pro Mixer Chip Interface command line utility to view and change mixer settings on compatible cards. Also includes, a program loadable/embeddable driver to control the mixer chip.</td></tr>
<tr><td>keywords</td><td>dos 16 bit file archive span split slice</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://gitlab.com/DOSx86/sbpmixer</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Mozilla Public License Version 2.0](LICENSE)</td></tr>
</table>
