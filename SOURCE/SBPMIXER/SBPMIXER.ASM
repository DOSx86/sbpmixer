;	Copyright 1993-2022, Jerome Shidel.
;
;	This project and related files are subject to the terms of the Mozilla
; Public License, v. 2.0. If a copy of the MPL was not distributed with this
; file, You can obtain one at http://mozilla.org/MPL/2.0/.
;
; v2.00 - Ported from Ancient Turbo Pascal Version to NASM

%ifndef MIXER_RET
	%define MIXER_CALL CALL FAR
	%define MIXER_RET  RETF
%endif

	Version:
		dw 		0x0200
	BasePort:
		dw		0x0220

    SetReg: ; { CH := Register & CL := Value }
      PUSH AX
      PUSH DX
      MOV  DX, [CS:BasePort]
      ADD  DX, 0x04                     ; { Add Offset to Mixer Chip Register}
      MOV  AL, CH
      OUT  DX, AL
      INC  DX
      MOV  AL, CL
      OUT  DX, AL
      POP  DX
      POP  AX
      RET

    GetReg: ; { CH := Register; CL = Value }
      PUSH AX
      PUSH DX
      MOV  DX,[CS:BasePort]
      ADD  DX, 0x04                     ; { Add Offset to Mixer Chip Register}
      MOV  AL, CH
      OUT  DX, AL
      INC  DX
      IN   AL, DX
      MOV  CL, AL
      POP  DX
      POP  AX
      RET

	SBPMixer:							; Far Call Entry to driver
	  XCHG  AX, BX
    SetOption:
      PUSHF
      PUSH CX
      PUSH BX
      PUSH DX
      MOV  CX, AX
      ; { Test for Functions >= 0x10 }
      CMP  BX, 0x10
      JNB  GetOption
      ; { Setting Functions < 0x10 }
      CMP  BX, 0x00      ; { Get Version }
      JE   GetVersion
      CMP  BX, 0x01
      JE   GetBasePort
      CMP  BX, 0x02      ; { Set BasePort }
      JE   SetBasePort
      CMP  BX, 0x03
      JE   ResetMixer
      CMP  BX, 0x08
      JNA  SetVolume
      CMP  BX, 0x09
      JE   SetFMChannel
      CMP  BX, 0x0A
      JE   SetStereoMode
      CMP  BX, 0x0B
      JE   SetLineVolume
      CMP  BX, 0x0C
      JE   SetADCSource
      CMP  BX, 0x0D
      JE   SetADCFilter
      CMP  BX, 0x0E
      JE   SetANFIFilter
      CMP  BX, 0x0F
      JE   SetDNFIFilter
      JMP  GetOption

    GetVersion:   ; { function 0 }
      MOV  CX,[CS:Version]
      JMP  Done

    GetBasePort: ; { function 1 }
      MOV  CX,[CS:BasePort]
      JMP  Done

    SetBasePort: ; { function 2 }
      MOV  [CS:BasePort], CX
      JMP  Done

    ResetMixer: ; { function 3 }
      MOV  CX, 0x0000
      CALL SetReg
      JMP  Done

    SetVolume: ; { function 4 - 8 }
      AND  CL, 0x0F
      SHL  CH, 1
      SHL  CH, 1
      SHL  CH, 1
      SHL  CH, 1
      OR   CL, CH
      CMP  BX, 0x05 ; { VOC volume goes to first set for stereo }
      JE   SetVolume2
      CMP  BX, 0x07 ; { MIC volume goes to first set for mono }
      JA   SetVolume2
      OR   BX, 0x10 ; { add 0x10 which will become 0x20 for those that
                    ;  need second set for stereo VOLUME CONTROL }
    SetVolume2:
      SUB  BX, 3
      SHL  BX, 1
      MOV  CH, BL
      CALL SetReg
      JMP  Done

    SetFMChannel: ; { Function 9; bits 4 - 7; reg 6 }
      PUSH CX
      MOV  CH, 0x26
      CALL GetReg  ; { get stereo fm volume }
      MOV  BL, CL
      MOV  CH, 0x06
      CALL GetReg ; { get mono fm volume; tries to prevent pop sound }
      MOV  BH, CL
      POP  CX
      MOV  CH, 0x06
      AND  CL, 0x03
      SHL  CL, 1
      INC  CL
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      AND  BH, 0x0F
      OR   CL, BH
      CALL SetReg  ; { set channel & fm mono volume }
      MOV  CH, 0x26
      MOV  CL, BL
      CALL SetReg ; { restore origanal fm volume }
      JMP  Done

    SetStereoMode: ; { function A; bits 0 - 1; reg E}
      MOV  CH, 0x0E
      PUSH CX
      CALL GetReg
      MOV  BL, CL
      POP  CX
      AND  BL, 0xFC
      AND  CL, 0x01
      SHL  CL, 1
      OR   CL, BL
      CALL SetReg
      JMP  Done

    SetLineVolume: ; { function B; reg 2E }
      MOV  BX, 0x1A
      JMP  SetVolume

    SetADCSource:  ; { function C; bits 0 - 2; reg C }
      MOV  CH, 0x0C
      PUSH CX
      CALL GetReg
      MOV  BL, CL
      POP  CX
      AND  BL, 0xF8
      AND  CL, 0x03
      SHL  CL, 1
      OR   CL, BL
      CALL SetReg
      JMP  Done

    SetADCFilter: ; { function D; Bit 3; reg C }
      MOV  CH, 0x0C
      PUSH CX
      CALL GetReg
      MOV  BL, CL
      POP  CX
      AND  BL, 0xF7
      AND  CL, 0x01
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      OR   CL, BL
      CALL SetReg
      JMP  Done

    SetANFIFilter: ; { function E; Bit 5; Reg C }
      MOV  CH, 0x0C
      PUSH CX
      CALL GetReg
      MOV  BL, CL
      POP  CX
      AND  BL, 0xDF
      AND  CL, 0x01
      XOR  CL, 0x01
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      OR   CL, BL
      CALL SetReg
      JMP  Done

    SetDNFIFilter: ; { function F; Bit 5; Reg E }
      MOV  CH, 0x0E
      PUSH CX
      CALL GetReg
      MOV  BL, CL
      POP  CX
      AND  BL, 0xDF
      AND  CL, 0x01
      XOR  CL, 0x01
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      SHL  CL, 1
      OR   CL, BL
      CALL SetReg
      JMP  Done

    GetOption:
      CMP  BX, 0x15
      JB   GetVolume
      JE   GetFMChannel
      CMP  BX, 0x16
      JE   GetStereo
      CMP  BX, 0x17
      JE   GetLineVolume
      CMP  BX, 0x18
      JE   GetADCSource
      CMP  BX, 0x19
      JE   GetADCFilter
      CMP  BX, 0x1B
      JNA  GetANFIDNFIFilter

	  ; Exit with ErrorCode 0x45
      MOV  AX, 0x45
      POP  DX
      POP  BX
      POP  CX
      POPF
      STC
      MIXER_RET

    GetVolume: ; { function 0x10 - 0x14 }
      SUB  BX, 0x0F
      CMP  BX, 0x02 ; { VOC volume goes to first set for stereo }
      JE   GetVolume2
      CMP  BX, 0x04 ; { MIC volume goes to first set for mono }
      JA   GetVolume2
      OR   BX, 0x10
    GetVolume2:
      SHL  BX, 1
      MOV  CH, BL
      CALL GetReg
      MOV  CH, CL
      AND  CL, 0x0F
      SHR  CH, 1
      SHR  CH, 1
      SHR  CH, 1
      SHR  CH, 1
      JMP  Done

    GetFMChannel: ; { function 0x15 }
      MOV  CH, 0x26
      CALL GetReg
      MOV  BX, CX
      MOV  CH, 0x06
      CALL GetReg
      MOV  CH, 0x00
      AND  CL, 0xF0
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      PUSH CX
      MOV  CX, BX
      CALL SetReg
      POP  CX
      JMP  Done

    GetStereo: ; { function 0x16 }
      MOV  CH, 0x0E
      CALL GetReg
      MOV  CH, 0x00
      SHR  CL, 1
      AND  CL, 0x01
      JMP  Done

    GetLineVolume: ; { Function 0x17 }
      MOV  BX, 0x26
      JMP  GetVolume

    GetADCSource: ; { function 0x18 }
      MOV  CH, 0x0C
      CALL GetReg
      MOV  CH, 0x00
      SHR  CL, 1
      AND  CL, 0x03
      JMP  Done

    GetADCFilter: ; { function 0x19 }
      MOV  CH, 0x0C
      CALL GetReg
      MOV  CH, 0x00
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      AND  CL, 0x01
      JMP  Done

    GetANFIDNFIFilter: ; { function 0x1A & 0x1B }
      MOV  CH, BL
      SUB  CH, 0x19
      SHL  CH, 1
      ADD  CH, 0x0A
      CALL GetReg
      MOV  CH, 0x00
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      SHR  CL, 1
      AND  CL, 0x01
      XOR  CL, 0x01
      JMP Done

    Done:
      MOV  AX, CX
      POP  DX
      POP  BX
      POP  CX
      POPF
      CLC
      MIXER_RET

